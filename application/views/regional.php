<div class="konten-halaman">
  <section>
      <div class="judul-halaman">
        <h2><strong>Regional</strong></h2>
      </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="isi-halaman">
          <img src="<?php echo base_url(); ?>assets/images/regional.png" class="img-konten">
          <p>Untuk saat ini komunitas penyelenggara Motherschooling baru berada di Kota Bandung, Jawa Barat.
Selanjutnya, pada bulan November kami berencana akan membentuk tim regional di Kabupaten Pangandaran.
</p>
        </div>
      </div>
    </div>
  </section>

</div>
