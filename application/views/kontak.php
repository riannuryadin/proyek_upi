<div class="konten-halaman">
  <section>
    <div class="judul-halaman">
      <h2><strong>KONTAK KAMI</strong></h2>
      <p><strong>Kami menyambut dengan baik semua pertanyaan dan saran untuk Motherschooling Indonesia</strong></p>
    </div>
  </section>
  <section>
    <div class="container" style="margin-top: 40px;">
      <div class="wrapper">
        <div class="row">
          <div class="col-md-5">
            <div class="isi-halaman">
              <div class="row">
                <p><img src="<?php echo base_url(); ?>assets/images/map.png" class="img-kontak"> <strong>Jalan Dr. Setiabudi No.229 Bandung</strong></p>
              </div>
              <div class="row">
                <p><img src="<?php echo base_url(); ?>assets/images/telpon.png" class="img-kontak"> <strong>0896-5359-7916</strong></p>
              </div>
              <div class="row">
                <p><img src="<?php echo base_url(); ?>assets/images/email.png" class="img-kontak"> <strong>motherschooling.indonesia@gmail.com</strong></p>
              </div>
            </div>
          </div>
          <div class="col-md-7">
            <?php echo form_open('Kontak/kirim_email'); ?>
            <input type="text" name="fullname" placeholder="Full name" class="form-kontak" required>
            <input type="email" name="email" placeholder="Email" class="form-kontak" required>
            <input type="text" name="phone" placeholder="Phone number" class="form-kontak" required>
            <input type="text" name="company" placeholder="Company" class="form-kontak">
            <input type="text" name="subject" placeholder="Subject" class="form-kontak" required>
            <textarea name="write" rows="7" cols="30" placeholder="Write" class="form-kontak" required></textarea>
            <input type="submit" name="submit" class="btn btn-kontak" value="SUBMIT">
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>

</div>
