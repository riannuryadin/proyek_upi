<div class="konten-halaman">
  <section>
      <div class="judul-halaman">
        <h2><strong>Visi Misi</strong></h2>
      </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="isi-halaman">
          <p><strong>Visi</strong></p>
          <p>Mencetak ibu cerdas berkualitas</p>
          <br>
          <p><strong>Misi</strong></p>
          <p>1.	Memberikan pengetahuan yang cukup terkait peran ibu dalam keluarga, melalui pembelajaran yang dikemas sesuai dengan kondisi peserta.</p>
          <p>2.	Memberikan pelatihan yang dapat diterapkan ibu calon ibu dalam membesarkan buah hati mereka.</p>
          <p>3.	Menciptakan forum diskusi interaktif dalam upaya memecahkan berbagai masalah ibu dan anak.</p>
        </div>
      </div>
    </div>
  </section>

</div>
