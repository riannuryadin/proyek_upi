<section>
  <div class="slide">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" >
        <!-- Carousel Indikator -->
        <ol class="carousel-indicators">
          <li data-target="carousel-example-generic" data-slide-to="0" class="active"></li>
          <li data-target="carousel-example-generic" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for Slide -->
        <div class="carousel-inner" style="height:547px;">
          <div class="item active">
              <img src="<?php echo base_url(); ?>assets/images/mom-and-child-reading.jpg" alt="Slide 1">
            </div>
            <div class="item">
              <img src="<?php echo base_url(); ?>assets/images/mom-and-child-reading2.jpg" alt="Slide 1">
            </div>
        </div>

        <!-- Control -->
        <a href="#carousel-example-generic" class="carousel-control left" data-slide="prev" role="button">
          <span class="glyphicon glyphicon-chevron-left"></span>
        </a>
        <a href="#carousel-example-generic" class="carousel-control right" data-slide="next" role="button">
          <span class="glyphicon glyphicon-chevron-right"></span>
        </a>
      </div>

    </div>
  </div>
</section>
<section>
  <div class="container">
    <div class="row">
      <div class="col-md-8 salam">
        <h2><strong>Salam dari Pendiri</strong></h2>
        <h4>Motherschooling Indonesia, salam ibu cerdas berkualitas!</h4>
        <p>Melalui program Motherschooling kami berharap dapat membagikan sedikit ilmu yang kami dapatkan dalam memahami pertumbuhan dan perkembangan anak, memahami psikologi anak, memahami bahwa setiap anak itu unik dan hebat. Memang kami belum mempunyai pengalaman langsung dalam mendidik dan membesarkan anak, tetapi kami dapat berbagi ilmu yang telah kami dapatkan dan para ibu dapat berbagi pengalaman yang telah para ibu alami. Para ibu atau calon ibu bersama dengan para pengajar yang kompeten di bidang nya, akan belajar mengenai materi-materi berikut ini:</p>
        <p>1.	Gizi yang baik untuk tumbuh kembang anak
        <br>2.	Tumpeng Gizi Seimbang dan cek pemenuhan nutrisi pada anak
        <br>3.	Zat aditif dalam makanan dan jajanan sehat untuk anak
        <br>4.	Uji boraks dan formalin dalam makanan
        <br>5.	Golden Age
        <br>6.	Tahap perkembangan pada anak
        <br>7.	Tipe-tipe kecerdasan pada anak
        <br>8.	Faktor-faktor yang mempengaruhi kecerdasan pada anak</p>
        <p>Besar harapan kami program Motherschooling dapat mencetak lebih banyak lagi ibu-ibu yang cerdas dan berkualitas dalam mendidik dan membesarkan buah hati mereka.</p>
      </div>
      <div class="col-md-4">
        <img src="" alt="">
      </div>
    </div>
  </div>
</section>
<section class="sec-testimoni">
  <div class="judul-section">
    <h2><strong>TESTIMONI</strong></h2>
  </div>
  <div class="testimoni">
    <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <!-- <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li> -->
      </ol>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active testimoni-text">
          <h4>"Ibu-ibu kader PKK dan ibu-ibu warga kelurahan Baros sangat berterimakasih, karena program Motherschooling menambah ilmu pengetahuan bagi mereka, bekal untuk mendidik dan membesarkan buah hati mereka."<br><span style="font-style:normal;">-Ani, Kasi Pemberdayaan Kel. Baros-</span></h4>
        </div>
        <!-- <div class="item testimoni-text">
          <h4>"One word... AMAZING!!"<br><span style="font-style:normal;">- Bill Gatos</span></h4>
        </div> -->
      </div>
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</section>
<section class="sec-mitra">
  <div class="judul-section">
    <h2><strong>MITRA YANG PERNAH TERLIBAT</strong></h2>
  </div>
  <div class="mitra">
    <div id="myCarousel" class="carousel slide text-center" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      </ol>
      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        <div class="item active testimoni-text">
          <img src="<?php echo base_url(); ?>assets/images/mitra/pkk-baros-cimahi.png" class="image-mitra">
        </div>
      </div>
      <!-- Left and right controls -->
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </div>
</section>
