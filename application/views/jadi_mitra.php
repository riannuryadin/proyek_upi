<div class="konten-halaman">
  <section>
    <div class="judul-halaman">
      <h2><strong>JADI MITRA</strong></h2>
      <p>Ayo Jadi Mitra <br>Mari terlibat langsung dalam kegiatan Motherschooling!</p>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="isi-halaman">
          <p>Mitra berperan sebagi penggerak para ibu-ibu atau wanita yang menjadi peserta Motherschooling. Selain itu mitra juga bertanggung jawab untuk menyediakan tempat dan perlengkapan yang dibutuhkan dalam pelaksanaan program Mothershooling. Mitra dapat berasal dari berbagai kalangan seperti ibu-ibu PKK, LSM, atau tokoh masyarakat lainnya. Bagi yang berminat menjadi mitra dapat mengisi form pengajuan kerjasama yang telah disediakan di bawah ini.</p>
        </div>
        <div class="coming-soon">
          <p>Form Mitra dapat diakses bulan November 2017</p>
        </div>
      </div>
    </div>
  </section>

</div>
