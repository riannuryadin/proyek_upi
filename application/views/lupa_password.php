<div class="konten-halaman">
  <section>
    <div class="judul-halaman">
      <h2><strong>Lupa Password</strong></h2>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="panes" style="width:300px; margin:auto;">
          <div class="pane">
            <?php echo form_open('lupa_password/kirim_password'); ?>
            <table>
              <tr>
                <td>Email : </td>
                <td><input type="email" name="email" style="text-size:12px;"></td>
              </tr>
              <tr>
                <td colspan="2"><input type="submit" name="submit" value="Kirim Password" class="btn btn-default" ></td>
              </tr>
            </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
