<div class="konten-halaman">
  <section>
    <div class="judul-halaman">
      <h2><strong>JADI VOLUNTEER</strong></h2>
      <p>Ayo Jadi Volunteer <br>Mari terlibat langsung dalam kegiatan Motherschooling!</p>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="isi-halaman">
          <p>Anda dapat berkontribusi dengan menjadi volunteer Instructor, Training Officer (TO), Publication and Documentation (PDD), Public Relation and Logistic (PRL). Bagi yang ingin menjadi Instructor harus memenuhi syarat sebagai mahasiswa atau lulusan perguruan tinggi yang berasal dari jurusan pendidikan, psikologi, biologi, ilmu gizi, kesehatan masyarakat, atau jurusan terkait lainnya yang mempelajari bidang materi yang terdapat dalam Motherschooling. Sedangkan untuk menjadi volunteer lainnya dapat berasal dari kalangan mahasiswa berbagai jurusan atau masyarakat umum dengan rentang usia 20-35 tahun. Bagi yang ingin bergabung menjadi volunteer akan melewati beberapa tahap seleksi yaitu seleksi berkas dan wawancara, khusus yang terpilih menjadi Instrcutor akan mendapat pelatihan mengajar sebelum terjun secara langsung mengajar.</p>
        </div>
        <div class="coming-soon">
          <p>Pendaftaran volunteer untuk Batch selanjutnya, dibuka bulan November, 2017.</p>
        </div>
      </div>
    </div>
  </section>

</div>
