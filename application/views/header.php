<!DOCTYPE html>

<html style="margin-top: 0!important;">

    <head>
        <meta charset="UTF-8">
        <title>Motherschooling Indonesia</title>

        <!-- STYLE -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css" />
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/static.css" type="text/css" />
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/MOTHERSCHOOLING.png">
        <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css'>
        <!--[if lt IE 9]>
        <script src="https://www.qwords.com/wp-content/themes/qwordsv7_theme/assets/js/html5.js"></script>
        <![endif]-->
        <style type="text/css">
            img.wp-smiley,
            img.emoji {
                display: inline !important;
                border: none !important;
                box-shadow: none !important;
                height: 1em !important;
                width: 1em !important;
                margin: 0 .07em !important;
                vertical-align: -0.1em !important;
                background: none !important;
                padding: 0 !important;
            }

        </style>
        <script type="text/javascript">
            function notifyMe() {
                if (!Notification) {
                    alert('Desktop notifications not available in your browser. Try Chromium.');
                    return;
                }
            }
            document.addEventListener('DOMContentLoaded', function () {
                if (Notification.permission !== "granted")
                    Notification.requestPermission();
            });
        </script>
        <link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/bootstrap.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/bootstrap-theme.min.css' type='text/css' media='all' />
        <link rel='stylesheet' href='<?php echo base_url(); ?>assets/plugins/jquery-ui-1.12.1/jquery-ui.css' type='text/css' media='all' />
        <link rel='stylesheet' href='<?php echo base_url(); ?>assets/css/extra.css' type='text/css' media='all' />




    </head>

    <body class="home blog" onload="notifyMe()">
        <!-- HEADER -->

        <header class="mastaka ">
            <!--bg-fix-->
            <div class="cep-top-bar container-fluid">
                <div class="pull-left call-center"></div>
                <div class="pull-right sosmed-tlp">
                    <?php if (!isset($_SESSION['email'])) { ?>
                        <div class="tlp daftar"> Hai, Selamat Datang | <a href="<?php echo base_url(); ?>index.php/login/" class="login-link">Login</a> <a href="<?php echo base_url(); ?>/index.php/login/" class="btn btn-default">Daftar</a></div>

                    <?php } else { ?>
                        <div class="tlp">Hai, Selamat Datang <?php echo $this->session->userdata('nama_depan'); ?> | <a href="<?php echo base_url(); ?>index.php/Akun/ubah_akun/" class="akun">Akun Saya</a> | <a href="<?php echo site_url('Login/logout');?>">Logout</a></div>
                    <?php } ?>
                    <div class="sosmed">
                        <ul>
                            <li>
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/images/a.png" style="width:20px;">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/images/b.png" style="width:20px;">
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/images/c.png" style="width:20px;">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="container-fluid menu__fix">
                <div class="logo pull-left"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" style="height:60px;"></a></div>
                <div class="cep-nav pull-right" style="height:10px;">
                    <ul id="menu-nav_qwords" class="menu">
                        <li id="menu-item-6558" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6558"><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/MOTHERSCHOOLING logo.png" style="height:30px;"></a>
                        </li>
                        <li id="menu-item-6551" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-nav"><a href="#">TENTANG KAMI</a>
                            <ul class="sub-menu">
                                <li id="menu-item-6552" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo base_url(); ?>index.php/Tentang_kami/visi_misi/">Visi Misi</a></li>
                                <li id="menu-item-7910" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo base_url(); ?>index.php/Tentang_kami/struktur_organisasi/">Struktur Organisasi</a></li>
                                <li id="menu-item-8504" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo base_url(); ?>index.php/Tentang_kami/regional/">Regional</a></li>
                                <li id="menu-item-8504" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo base_url(); ?>index.php/Tentang_kami/bagaimana_kami_bekerja/">Bagaimana Kami Bekerja</a></li>
                                <li id="menu-item-8504" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo base_url(); ?>index.php/Tentang_kami/liputan_media/">Liputan Media</a></li>
                                <li id="menu-item-8504" class="menu-item menu-item-type-custom menu-item-object-custom"><a href="<?php echo base_url(); ?>index.php/Tentang_kami/faq">FAQ</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-6553" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6553"><a href="#">MARI BERGABUNG</a>
                            <ul class="sub-menu">
                                <li id="menu-item-7948" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="<?php echo base_url(); ?>index.php/Mari_bergabung/jadi_volunteer/">Jadi Volunteer</a></li>
                                <li id="menu-item-7947" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="<?php echo base_url(); ?>index.php/Mari_bergabung/jadi_mitra/">Jadi Mitra</a></li>
                                <li id="menu-item-7946" class="menu-item menu-item-type-post_type menu-item-object-page "><a href="<?php echo base_url(); ?>index.php/Mari_bergabung/ikut_iuran/">Ikut Iuran</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-6563" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6563"><a href="#">KABAR TERBARU</a>
                            <ul class="sub-menu">
                                <li id="menu-item-7979" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7979"><a href="<?php echo base_url(); ?>index.php/Kabar_terbaru/berita/">Berita</a></li>
                                <li id="menu-item-7974" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7974"><a href="<?php echo base_url(); ?>index.php/Kabar_terbaru/galeri/">Galeri</a></li>
                            </ul>
                        </li>
                        <li id="menu-item-6554" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6554"><a href="<?php echo base_url(); ?>index.php/Merchandise/">MERCHANDISE</a>
                        </li>
                        <li id="menu-item-6558" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-6558"><a href="<?php echo base_url(); ?>index.php/Kontak/">KONTAK</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- HEADER -->
