<div class="konten-halaman">
  <section>
    <div class="judul-halaman">
      <h2><strong>JADI MITRA</strong></h2>
      <p>Cara Motherschooling Indonesia mengajak masyarakat berkontribusi <br> dalam mendanai pelaksanaan program Motherschooling di berbagai daerah di Indonesia</p>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="isi-halaman">
          <img src="<?php echo base_url(); ?>assets/images/ikut_iuran.png" class="img-konten">
          <p>Pertanyaan dan saran dapat disampaikan melalui: <br>
            E-mail: motherschooling.indonesia@gmail.com <br>
            Telepon: 0896-5359-7916
          </p>
        </div>
      </div>
    </div>
  </section>

</div>
