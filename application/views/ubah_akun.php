<div id='content' class="konten-halaman">
	<div class='wrapper'>
		<h1>Ubah Profil</h1>
		<div class='profile-pic'>
			<a href="#">
          <img src = '<?php echo base_url(); ?>assets/images/profile_default.jpg' id = 'profile-pic'>
      </a>
			<form enctype="multipart/form-data" method='POST' action="/my-account/change-picture/">
				<div style='display:none'>
          <input type='hidden' name='csrfmiddlewaretoken' value='U7k5dJHczial0bsAT77hEz1IEn8EcAaH' />
        </div>
				<table class='picture-table'>
					<tr>
						<td class="rowLabel">Ubah foto:</td>
					</tr>
					<tr>
            <td class="formRow">
  						<div class="">
  							<input type="file" name="mugshot" id="id_mugshot" />
  							<span class='help-text'>JPG atau PNG. Ukuran max. 50KB</span>
  						</div>
  					</td>
					</tr>
					<tr>
						<td class='fake'>
							<button class='fakebutton' type='button'>Choose file</button><span class='faketext'>No file chosen</span>
						</td>
					</tr>
				</table>
				<button class='medium-button' disabled='disabled'><span>Simpan foto</span></button>
			</form>
    </div>
    <!-- end Profile pict -->
		<div id='profile-wrapper'>
			<div class="panes">
				<div class='pane'>
					<?php $data = array(
						'id' => $id
					);
					?>
					<?php echo form_open('Akun/ubah_data_profile/','', $data); ?>
						<div style='display:none'><input type='hidden' name='csrfmiddlewaretoken' value='U7k5dJHczial0bsAT77hEz1IEn8EcAaH' /></div>
						<table class='profile-table change-profile'>
							<tr>
								<td class="rowLabel"><em>*</em>Nama Lengkap</td>
								<td class="formRow">
									<div class="name">
										<input id="id_first_name" type="text" class="name" value="<?php echo $nama_depan; ?>" name="nama_depan" />
										<span class='help-text'>Nama depan</span>
									</div>
									<div class="name">
										<input id="id_middle_name" type="text" class="name" value="<?php echo $nama_tengah; ?>" name="nama_tengah" />
										<span class='help-text'>Nama tengah (optional)</span>
									</div>
									<div class="name">
										<input id="id_last_name" type="text" class="name" value="<?php echo $nama_belakang; ?>" name="nama_belakang" />
										<span class='help-text'>Nama belakang (optional)</span>
									</div>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Nama Panggilan</td>
								<td class="formRow">
									<div class="">
										<input type="text" name="nama_panggilan" id="id_nick_name" value="<?php echo $nama_panggilan; ?>" />
									</div>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Tanggal Lahir</td>
								<td class="formRow">
									<div class="">
										<input type="text" name="tanggal_lahir" class="input-tanggal" value="<?php echo $tanggal_lahir; ?>">
									</div>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Jenis Kelamin</td>
								<td class="formRow">
									<?php
									$laki ='';
									$perempuan ='';
									 if($jenis_kelamin == 1){
										$laki = 'checked';
									}else if($jenis_kelamin == 2){
										$perempuan = 'checked';
									}
									?>
									<div class="">
										<ul>
											<li><label for="id_gender_1"><input type="radio" id="id_gender_1" value="1" name="jenis_kelamin" <?php echo $laki; ?> /> Laki-laki</label></li>
											<li><label for="id_gender_2"><input type="radio" id="id_gender_2" value="2" name="jenis_kelamin" <?php echo $perempuan; ?> /> Perempuan</label></li>
										</ul>
									</div>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Facebook profile</td>
								<td class="formRow">
									<div class="">
										<input type="text" name="facebook_profile" id="id_facebook" value="<?php echo $facebook_profile; ?>" />
										<span class='help-text'>Contoh: http://facebook.com/pengajarmuda</span>
									</div>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Twitter profile</td>
								<td class="formRow">
									<div class="">
										<input type="text" name="twitter_profile" id="id_twitter" value="<?php echo $twitter_profile; ?>" />
										<span class='help-text'>Contoh: http://twitter.com/pengajarmuda</span>
									</div>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Linkedin profile</td>
								<td class="formRow">
									<div class="">
										<input type="text" name="linkedin_profile" id="id_linked_in" value="<?php echo $linkedin_profile; ?>" />
										<span class='help-text'>Contoh: id.linkedin.com/in/contoh</span>
									</div>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Website</td>
								<td class="formRow">
									<div class="">
										<input type="text" name="web_site" id="id_website" value="<?php echo $web_site; ?>"/>
									</div>
								</td>
							</tr>
						</table>
						<input type='submit' value='Submit'>
					</form>
				</div>
				<div class='pane changepassword_pane'>
					<div class="title">Email</div>
					<?php echo form_open('Akun/ubah_email/','',$data ); ?>
						<div style='display:none'><input type='hidden' name='csrfmiddlewaretoken' value='U7k5dJHczial0bsAT77hEz1IEn8EcAaH' /></div>
						<table class='profile-table change-profile change-email'>
							<tr>
								<td class="rowLabel">
									<em>*</em>Email
								</td>
								<td class="formRow">
									<input type="text" value="<?php echo $email; ?>" disabled/>
									<span class="help-text">Gunakan email valid untuk menerima kabar terbaru dari Indonesia Mengajar.</span>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Ubah Email</td>
								<td class="formRow">
									<div class="required">
										<input id="id_email" type="text" class="required" name="email" maxlength="75" />
									</div>
								</td>
							</tr>
						</table>
						<div class="border_line">
							<button class="medium-button">
            		<span>Simpan Perubahan</span>
        			</button>
						</div>
					</form>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<div class="title">Password</div>
					<?php echo form_open('Akun/ubah_password','',$data); ?>
						<div style='display:none'><input type='hidden' name='csrfmiddlewaretoken' value='U7k5dJHczial0bsAT77hEz1IEn8EcAaH' /></div>
						<table class='profile-table change-profile'>
							<tr>
								<td class="rowLabel">Password Lama</td>
								<td class="formRow">
									<input id="id_password" type="password" name="password" class="passwordinput" value="<?php echo $password; ?>"/>
								</td>
							</tr>
							<tr>
								<td class="rowLabel">Password Baru</td>
								<td class="formRow">
									<input id="id_change_password" type="password" name="password" class="passwordinput" />
								</td>
							</tr>
						</table>
						<div class="border_line">
							<button class="medium-button">
                <span>Simpan Perubahan</span>
              </button>
						</div>
					</form>
					<div>&nbsp;</div>
					<div>&nbsp;</div>
					<div class="title">Notifikasi Email</div>
					<?php echo form_open('Akun/ubah_email_notif/','',$data); ?>
						<div style='display:none'><input type='hidden' name='csrfmiddlewaretoken' value='U7k5dJHczial0bsAT77hEz1IEn8EcAaH' /></div>
						<table class='profile-table change-notification'>
							<tr>
								<td class="rowLabel"></td>
								<td>
									<?php
									$notif = '';
									 if($email_notif == 1){
										 $notif = 'checked';
									}
									?>
									<input type="checkbox" name="email_notif" id="id_email_notification" <?php echo $notif; ?> value="1" /> Nyalakan notifikasi email
								</td>
							</tr>
							<tr>
								<td class="formRow"></td>
								<td>
									<span class="help-text">Nyalakan untuk menerima notifikasi terbaru di emailmu.</span>
								</td>
							</tr>
						</table>
						<div class="border_line">
							<button class="medium-button">
            <span>Simpan Perubahan</span>
        </button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
