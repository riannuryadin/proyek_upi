<div id = 'footer-bottom' style="background:#ddd;">
  <div class="row">
    <div class="col-md-4">

    </div>
    <div class="col-md-4">
      <div class="foot-copyright">
        <p>&copy; 2017 <strong>Motherschooling Indonesiar</strong></p>
      </div>
    </div>
    <div class="col-md-4">
      <div class="foot-support">
        <p>Didukung oleh : <img src="<?php echo base_url(); ?>assets/images/Logo_Kemenristekdikti.png" style="width:40px;"></p>
      </div>
    </div>
  </div>
</div>

<script src="<?php echo base_url(); ?>assets/js/pkm.js"></script>

<script src="<?php echo base_url(); ?>assets/js/pkm-plug.js"></script>
<script src="<?php echo base_url(); ?>assets/js/app.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui-1.12.1/jquery-ui.js"></script>

<!-- testimoni -->
<script type="text/javascript">
  $('#myCarousel').carousel({
    interval: 4000
  });
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('.input-tanggal').datepicker();
	});
</script>

</body>

</html>
