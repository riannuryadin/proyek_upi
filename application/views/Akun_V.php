<div class="konten-halaman">
  <body class = "login-page">

      <div id = 'content'>

          <?php if (isset($pesan)) { ?>
          <div class="alert alert-danger alert-dismissable  "><p style="text-align : center;"><?php echo $pesan; } ?></p> </div>
              <?php
              if (validation_errors() != null) {
                  ?>
          <div class="alert alert-warning" role="alert"><p style="text-align : center;"><?php echo validation_errors(); ?></p></div>
                      <?php
                  }
                  ?>
                  <div class = 'wrapper'>
                      <div class = 'login-container'>
                          <div class = 'login-wrapper'>
                              <h1>Login</h1>
                              <form action = '<?php echo site_url('Login/cek_login');?>' method = 'post'>

                                  <div style='display:none'><input type='hidden' name='csrfmiddlewaretoken' value='kXlUhc5dgInNXEmzXVoMi49lrg5haqks' /></div>
                                  <input id="id_identification" type="text" placeholder="Email" name="email" maxlength="30" /></td></tr>
                                  <input type="password" placeholder="Password" name="password" id="id_password" /></td></tr>
                                  <a href="<?php echo base_url(); ?>index.php/lupa_password/"><label>lupa password?</label></a><br>
                                  <button class = 'button cta login'><span>Login</span></button>
                              </form>
                          </div>


                          <div class = 'login-wrapper register'>
                              <h1>Buat akun baru</h1>
                              <form action = '<?php echo site_url('Akun/menambahkan_akun'); ?>' method = 'post'>
                              <div style='display:none'><input type='hidden' name='csrfmiddlewaretoken' value='kXlUhc5dgInNXEmzXVoMi49lrg5haqks' /></div>

                              <div class = 'first_name'>
                                  <input type="text" placeholder="Nama depan"name="nama_depan" id="id_nama_depan" required="nama depan tidak boleh kosong"/>
                              </div>

                              <div class = 'email'>
                                  <input class="coba" type="text" placeholder="Email" name="email" id="id_email" required="email tidak boleh kosong" />
                              </div>

                              <div class = 'password1'>

                                  <input type="password" placeholder="Password" name="password" id="id_password1"  required="password tidak boleh kosong"/>
                              </div>

                              <div class = 'password2'>
                                  <input class="coba" type="password" placeholder="Konfirmasi Password" name="password1" id="id_password1" required="konfirmasi password tidak boleh kosong"/>
                              </div>

                              <div class="clear"></div>
                              <button class = 'button cta register' name="daftar"><span>Daftar</span></button>
                          </form>
                      </div>
                  </div>


              </div>
          </div>


</div>
