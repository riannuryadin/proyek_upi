<div class="konten-halaman">
  <section>
      <div class="judul-halaman">
        <h2><strong>Struktur Organisasi</strong></h2>
      </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="isi-halaman">
          <img src="<?php echo base_url(); ?>assets/images/struktur_organisasi.png" class="img-konten">
          <p>Pendiri sekaligus tim inti Motherschooling Indonesia yakni Agung Nugraha sebagai Ketua dan Penanggung Jawab Program, Mega Laeni sebagai Sekretaris dan Penanggung Jawab Volunteer, Rizki Ayu Kania sebagai Bendahara dan Penanggung Jawab Mitra, Recky Anadi sebagai Penanggung Jawab Publikasi dan Dokumentasi, dan Dicky Aprisandi sebagai Penanggung Jawab Humas dan Logik.</p>
          <p>Tim inti Motherschooling Indonesia memerlukan bantuan dalam melaksanakan program di daerah-daerah yang tidak dapat tim inti jangkau, untuk itu dibentuk tim regional guna memudahkan pengelolaan dan pelaksanan Motherschooling. Tim regional diketuai oleh volunteer Motherschooling yang sudah memiliki pengalaman dalam melaksanakan program Motherschooling sebelumnya. Tim regional dibantu oleh volunteer yang terpilih dalam melaksanakan program Motherschooling.</p>
        </div>
      </div>
    </div>
  </section>

</div>
