<div class="konten-halaman">
  <section>
    <div class="judul-halaman">
      <h2><strong>Bagaimana Kami Bekerja</strong></h2>
    </div>
  </section>
  <section>
    <div class="container">
      <div class="wrapper">
        <div class="isi-halaman">
          <p>Dalam Upaya mencapai tujuan, Motherschooling Indonesia melakukan proses pengelolaan dan pelaksanaan program secara terencana. Proses pengelolaan dan pelaksanaan program dijalankan oleh tim Inti Motherschooling Indonesia dan volunteer. Berikut adalah aktivitas dalam pengelolaan dan pelaksanaan program Motherschooling:</p>
          <img src="<?php echo base_url(); ?>assets/images/bagaimana_kami_bekerja.png" class="img-konten">
        </div>
      </div>
    </div>
  </section>

</div>
