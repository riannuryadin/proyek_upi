<?php

Class Mari_bergabung extends CI_Controller{
  public function jadi_volunteer(){
    $this->load->view('header');
    $this->load->view('jadi_volunteer');
    $this->load->view('footer');
  }

  public function jadi_mitra(){
    $this->load->view('header');
    $this->load->view('jadi_mitra');
    $this->load->view('footer');
  }

  public function ikut_iuran(){
    $this->load->view('header');
    $this->load->view('ikut_iuran');
    $this->load->view('footer');
  }
}

 ?>
