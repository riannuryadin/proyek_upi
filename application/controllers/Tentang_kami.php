<?php

Class Tentang_kami extends CI_Controller{
  public function visi_misi(){
    $this->load->view('header');
    $this->load->view('visi_misi');
    $this->load->view('footer');
  }

  public function struktur_organisasi(){
    $this->load->view('header');
    $this->load->view('struktur_organisasi');
    $this->load->view('footer');
  }

  public function regional(){
    $this->load->view('header');
    $this->load->view('regional');
    $this->load->view('footer');
  }

  public function bagaimana_kami_bekerja(){
    $this->load->view('header');
    $this->load->view('bagaimana_kami_bekerja');
    $this->load->view('footer');
  }

  public function liputan_media(){
    $this->load->view('header');
    $this->load->view('liputan_media');
    $this->load->view('footer');
  }

  public function faq(){
    $this->load->view('header');
    $this->load->view('footer');
  }
}


 ?>
