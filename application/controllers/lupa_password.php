<?php

Class Lupa_password extends CI_Controller{

  function __contruction() {
      parent::__construct();
      $this->load->model('Akun_M');
  }

  public function index(){
    $this->load->view('header');
    $this->load->view('lupa_password');
    $this->load->view('footer');
  }

  function kirim_password(){
    $email = $this->input->post('email');
    $data = $this->Akun_M->get_password($email);
    $this->email->to($email);
    $this->email->from('motherschooling.indonesia@gmail.com', 'Moherschooling Indonesia');
    $this->email->subject('Lupa Password Motherschooling Indonesia');
    $this->email->message('Password untuk email : '.$email.' adalah '.$data[0]['password']);
    $this->email->send();
  }

}


 ?>
