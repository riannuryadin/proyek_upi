<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Daftar_akun
 *
 * @author Riman Nuryadin
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Akun extends CI_Controller {

    function __costruct() {
        parent::__construct();
        $this->load->model('Akun_M');
        $this->load->model('coba');
        $this->load->library('javascript');
    }

    function menambahkan_akun() {
        $nama_depan = $this->input->post('nama_depan');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $password1 = $this->input->post('password1');

        $data = array(
            'email' => $email,
            'password' => $password,
            'nama_depan' => $nama_depan
        );

        $result = $this->Akun_M->ambil_email_akun($email);
        if ($result == false) {
            if (strlen($password) > 4) {
                if ($password == $password1) {
                    $this->Akun_M->menambahkan_akun($data);
                    $pesan['pesan'] = "selamat anda berhasil membuat akun silahkan lakukan login";
                    $this->load->view('header');
                    $this->load->view('Akun_V', $pesan);
                    $this->load->view('footer');
                } else {

                    $pesan['pesan'] = " mohon maaf password tidak sama";
                    $this->load->view('header');
                    $this->load->view('Akun_V', $pesan);
                    $this->load->view('footer');
                }
            } else {

                $pesan['pesan'] = " mohon maaf password harus lebih dari 4 karakter";
                $this->load->view('header');
                $this->load->view('Akun_V', $pesan);
                $this->load->view('footer');
            }
        } else {
            $pesan['pesan'] = " email yang anda gunakan sudah ada silahkan lakukan login";
            $this->load->view('header');
            $this->load->view('Akun_V', $pesan);
            $this->load->view('footer');
        }
    }

    function ubah_akun() {
      $data = array(
        'email' => $this->session->userdata('email'),
        'password' => $this->session->userdata('password')
      );
      $akun = $this->Akun_M->cek_login_member($data);
      foreach ($akun->result() as $res) {
        $data['id'] = $res->id;
        $data['nama_depan']         = $res->nama_depan;
        $data['nama_tengah']        = $res->nama_tengah;
        $data['nama_belakang']      = $res->nama_belakang;
        $data['nama_panggilan']     = $res->nama_panggilan;
        $tgl                        = $res->tanggal_lahir;
        $pisah                      = explode('-',$tgl);
        $larik                      = array($pisah[1], $pisah[2], $pisah[0]);
        $data['tanggal_lahir']      = implode('/',$larik);
        $data['jenis_kelamin']      = $res->jenis_kelamin;
        $data['facebook_profile']   = $res->facebook_profile;
        $data['twitter_profile']    = $res->twitter_profile;
        $data['linkedin_profile']   = $res->linkedin_profile;
        $data['web_site']           = $res->web_site;
        $data['foto']               = $res->foto;
        $data['email']              = $res->email;
        $data['password']           = $res->password;
        $data['email_notif']        = $res->email_notif;
      }
      $this->load->view('header');
      $this->load->view('ubah_akun', $data);
      $this->load->view('footer');

    }

    function ubah_data_profile(){
      $id                         = $this->input->post('id');
      $data['nama_depan']         = $this->input->post('nama_depan');
      $data['nama_tengah']        = $this->input->post('nama_tengah');
      $data['nama_belakang']      = $this->input->post('nama_belakang');
      $data['nama_panggilan']     = $this->input->post('nama_panggilan');
      $tgl                        = $this->input->post('tanggal_lahir');
      $pisah                      = explode('/',$tgl);
      $larik                      = array($pisah[2], $pisah[0], $pisah[1]);
      $data['tanggal_lahir']      = implode('-',$larik);
      $data['jenis_kelamin']      = $this->input->post('jenis_kelamin');
      $data['facebook_profile']   = $this->input->post('facebook_profile');
      $data['twitter_profile']    = $this->input->post('twitter_profile');
      $data['linkedin_profile']   = $this->input->post('linkedin_profile');
      $data['web_site']           = $this->input->post('web_site');
      $this->Akun_M->ubah_data_profile($data, $id);
      $this->session->set_userdata('nama_depan', $data['nama_depan']);
      redirect('Akun/ubah_akun');
    }

    function ubah_email(){
      $id    = $this->input->post('id');
      $email = $this->input->post('email');
      $this->Akun_M->ubah_email($email, $id);
      $this->session->set_userdata('email', $email);
      redirect('Akun/ubah_akun');
    }

    function ubah_password(){
      $id       = $this->input->post('id');
      $password = $this->input->post('password');
      $this->Akun_M->ubah_password($password, $id);
      $this->session->set_userdata('password', $password);
      redirect('Akun/ubah_akun');
    }

    function ubah_email_notif(){
      $id           = $this->input->post('id');
      $email_notif  = $this->input->post('email_notif');
      $this->Akun_M->ubah_email_notif($email_notif, $id);
      redirect('Akun/ubah_akun');
    }


}
