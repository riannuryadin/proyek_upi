<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Login
 *
 * @author Rian Nuryadin
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {

    function __contruction() {
        $this->load->model('Akun_M');
    }

    function index() {
        $this->load->view('header');
        $this->load->view('Akun_V');
        $this->load->view('footer');
    }

    function cek_login() {
        $data = array(
            'email' => $this->input->post('email'),
            'password' => $this->input->post('password')
        );


        $member = $this->Akun_M->cek_login_member($data);
        $admin = $this->Akun_M->cek_login_admin($data);

        if ($member->num_rows() == 1) {
            foreach ($member->result()as $sess) {
                $sess_data['email'] = $sess->email;
                $sess_data['password'] = $sess->password;
                $sess_data['nama_depan'] = $sess->nama_depan;
                $session = $this->session->set_userdata($sess_data);
                redirect('home');
            }
        } else if ($admin->num_rows() == 1) {
            foreach ($admin->result()as $sess) {
                $sess_data['email'] = $sess->email;
                $sess_data['password'] = $sess->password;
                $sess_data['nama_depan'] = $sess->nama_depan;
                $session = $this->session->set_userdata($sess_data);
                redirect('home');
            }
        } else {
            $this->load->view('Header');
            $data['pesan'] = "Email dan password yang anda masukan tidak benar";
            $this->load->view('Akun_V', $data);
            $this->load->view('footer');
        }
    }

    function logout() {
        $this->session->unset_userdata('email');
        $this->session->sess_destroy();
        redirect('Login/index');
    }

}
