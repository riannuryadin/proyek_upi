<?php

Class Kontak extends CI_Controller{
  public function index(){
    $this->load->view('header');
    $this->load->view('kontak');
    $this->load->view('footer');
  }

  public function kirim_email(){
    $fullname = $this->input->post('fullname');
    $email = $this->input->post('email');
    $subject = $this->input->post('subject');
    $write = $this->input->post('write');

    $this->email->to('motherschooling.indonesia@gmail.com');
    $this->email->from($email, $fullname);
    $this->email->subject($subject);
    $this->email->message($write);
    $this->email->send();
    redirect('Kontak');
  }
}

 ?>
